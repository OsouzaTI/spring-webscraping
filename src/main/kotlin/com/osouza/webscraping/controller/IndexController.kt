package com.osouza.webscraping.controller

import com.osouza.webscraping.service.MangaYabu
import org.jsoup.Jsoup
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping


@Controller
@RequestMapping("/")
class IndexController {

    @Autowired
    private lateinit var mangaScraper : MangaYabu

    @GetMapping
    fun index(model : ModelMap) : String {
        model.addAttribute("mangas", mangaScraper.index())
        return "index"
    }


}