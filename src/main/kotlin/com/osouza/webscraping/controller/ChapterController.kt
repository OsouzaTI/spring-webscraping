package com.osouza.webscraping.controller

import com.osouza.webscraping.service.MangaYabu
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam

@Controller
@RequestMapping("/chapter")
class ChapterController {

    @Autowired
    private lateinit var mangaScraper : MangaYabu

    @GetMapping("{slug}")
    fun index(@PathVariable slug : String, model : ModelMap) : String {
        val capitulos = mangaScraper.chapters(slug)
        model.addAttribute("capitulos", capitulos)
        return "chapter"
    }

}