package com.osouza.webscraping.types.interfaces

import com.osouza.webscraping.types.model.ChapterModel
import com.osouza.webscraping.types.model.MangaModel

interface MangaScaperInterface {

    fun index() : List<MangaModel>
    fun chapters(manga: String) : List<ChapterModel>
    fun pages(chapter: String) : List<String>

}