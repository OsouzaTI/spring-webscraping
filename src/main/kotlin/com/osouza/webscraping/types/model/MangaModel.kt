package com.osouza.webscraping.types.model

data class MangaModel(val title : String, val subtitle: String, val image : String, val link : String)
