package com.osouza.webscraping.types.model

data class ChapterModel(val title : String, val link : String)
