package com.osouza.webscraping.types.generics

import com.osouza.webscraping.types.interfaces.MangaScaperInterface
import org.jsoup.Jsoup
import org.jsoup.nodes.Document


abstract class AbstractMangaScraper(private val target : String) : MangaScaperInterface {
    val startpoint : String = target
    var doc : Document = Jsoup.connect(target).get()
}