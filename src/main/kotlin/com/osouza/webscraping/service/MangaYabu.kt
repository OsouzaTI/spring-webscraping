package com.osouza.webscraping.service

import com.osouza.webscraping.types.generics.AbstractMangaScraper
import com.osouza.webscraping.types.model.ChapterModel
import com.osouza.webscraping.types.model.MangaModel
import org.jsoup.Jsoup
import org.springframework.stereotype.Service

@Service
class MangaYabu : AbstractMangaScraper("https://mangayabu.top") {

    override fun index(): List<MangaModel> {

        val elements = doc.select(".manga-rl-grid > div.manga-item")
        val mangas : List<MangaModel> = elements.map { it -> MangaModel(
            it.select("a").attr("title"),
            "subtitle",
            it.select("img").attr("src"),
            it.select("a").attr("href").split("/")[4]
        )}

        return mangas

    }

    override fun chapters(manga: String): List<ChapterModel> {
        println(manga)
        doc = Jsoup.connect("${this.startpoint}/manga/$manga").get()

        val elements = doc.select("#chapters-list > div.manga-index")
        val chapters : List<ChapterModel> = elements.map { it -> ChapterModel(
            it.select("a").attr("title"),
            it.select("a").attr("href"),
        )}

        println(chapters)

        return chapters

    }

    override fun pages(chapter: String): List<String> {
        return emptyList()
    }


}