package com.osouza.webscraping

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class WebscrapingApplication

fun main(args: Array<String>) {
	runApplication<WebscrapingApplication>(*args)
}
